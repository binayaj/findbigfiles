## Details
Sort all files according to their size in a given path or folder recursively in terms of bytes. The path can be / too.

Highly efficient (in terms of CPU utiliztion and time) as compared to running 'find' and 'sort' from command prompt, when we need to process a folder or path with massive volumes of files.

## Usage
```
python findbigfiles.py path(folder) > outputfile

Eg:


python findbigfiles.py / > outputfile

Result: 
140737486266368 /proc/kcore
10737418240 /root/temp.img
....


```
## Note
If you don''t redirect output, then it will print results on the screen. Also, if any error is encountered while accessing files, it is logged to an error file in the working directory.

## Author
Binaya Joshi
