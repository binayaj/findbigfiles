#!/usr/bin/env python
#Author : Binaya Joshi

import os,sys,time,subprocess

if (len (sys.argv) == 2):
	if (os.path.isdir(sys.argv[1])):
		#Generate unique filenames
		rand_str=str(time.time())
		errorfile="error_report"+rand_str+".txt"
		tempfile="tempfile"+rand_str+".tmp"
		with open (tempfile,'w') as fp,  open(errorfile, 'w') as fp1:
			# Traverse recursively through the directory.
			for dir, subdir, files in os.walk(sys.argv[1]):
				for file in files:
					path = os.path.join(dir, file)
					try:
                                          # Exclude links and save size of all files
						if (not os.path.islink(path)):
							size = os.stat(path).st_size
							fp.write ("{0}" " {1}\n".format (size,path))
					except OSError as e: 
						fp1.write("Error encountered while accessing {}\n".format(path))
		fp1.close()
		fp.close()		   
		#sort the contents of files based on their size 
		p1 = subprocess.call(['sort', '-nr',tempfile])
		os.remove (tempfile)
                if (os.stat(errorfile).st_size==0):
                     os.remove(errorfile)
	else:
		print ("Directory doesn't exist")
		exit(1)
else: 
	print("Usage: {} directory_name".format (sys.argv[0]))
